@extends('layouts.adm') 
@section('content')

<!-- BEGIN CONTENT BODY -->
                <div class="page-content-wrapper">
                    <div class="content-wrapper container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title">

                                    <h4 class="float-left">Create spec</h4>

                                    <ol class="breadcrumb float-left float-md-right">
                                        <li class="breadcrumb-item"><a href="/spec"><i class="fa fa-arrow-left"></i></a></li>

                                    </ol>

                                </div>
                            </div>
                        </div><!-- end .page title-->

				
								<div class="col-md-12">
                                <div class="panel panel-card margin-b-30">
                                    <!-- Start .panel -->
                                    <div class="card-header">
                                        spec
                                    </div>
                                    <div class="panel-body  p-xl-3">
                                        <form  action="{{ route('spec.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                            {{ csrf_field() }}
											

                                            <div class="form-group row"><label class="col-lg-2 form-control-label">Category</label>

                                            <div class="col-lg-3">
												<select name="cat_id" id="cat_id" class="form-control select" required>
												<option value="">-- PILIH category --</option>
												@foreach ($category as $item)
													<option value="{{ $item->id }}">{{ $item->category}}</option>
												@endforeach
												</select>
												</div>
											</div>

											<div class="form-group row">
                                            <label class="col-lg-2 form-control-label">Spec :</label>
                             
                                            <div class="col-lg-4">          
                                            <table class="col-lg-10" id="dynamic_field">  
                                            <tr>  
                                            <td><input required type="text" name="name[]" placeholder="" class="form-control name_list" /></td>  
                                            <td><button type="button" name="add" id="add" class="btn btn-success">+</button></td>  
                                            </tr> 
                                            </table>

                            
                                            </div>

											
                                            <div class="hr-line-dashed"></div>
                                            <div class="col-lg-6">

                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4 col-sm-offset-2">
                                                    <button class="btn btn-primary" type="submit">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
				
				
				
<script>
 $(document).ready(function(){      
 var postURL = "<?php echo url('addmore'); ?>";
var i=1;  
            
            
$('#add').click(function(){  
  i++;  
 $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input required type="text" name="name[]" placeholder="" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
 });  
            
            
$(document).on('click', '.btn_remove', function(){  
var button_id = $(this).attr("id");   
$('#row'+button_id+'').remove();  
});  
 });  
</script>		

<script>
	
</script>		
				
				
				
				
            </div>
            <!-- END CONTAINER -->
        </div>
        <!-- /wrapper -->
		
		 

        <!-- SCROLL TO TOP -->
        <a href="#" id="toTop"></a> 
	@endsection