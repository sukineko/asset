@extends('layouts.adm') 
@section('content')

<!-- BEGIN CONTENT BODY -->
                <div class="page-content-wrapper">
                    <div class="content-wrapper container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title">

                                    <h4 class="float-left">Create category</h4>

                                    <ol class="breadcrumb float-left float-md-right">
                                        <li class="breadcrumb-item"><a href="/category"><i class="fa fa-arrow-left"></i></a></li>
                                    
                                    </ol>

                                </div>
                            </div>
                        </div><!-- end .page title-->

                            <div class="col-md-12">


                                <div class="col-md-6">
                                <div class="panel panel-card margin-b-30">
                                    
                                    <div class="panel-body  p-xl-3">
                                        <form class="form-horizontal" action="{{ route('category.store') }}" method="post">
										{{ csrf_field() }}
										
											<input type="hidden" name="user_id" class="form-control" value="{{ Auth::user()->id }}">
										
                                            <div class="form-group row"><label class="col-lg-2 form-control-label">Name</label>

                                                <div class="col-lg-10">
												<input type="text"  name="category" placeholder="Name" class="form-control" required> 
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-lg-2 form-control-label">Spec :</label>
                                 
                                                <div class="col-lg-10">          
                                                <table class="col-lg-10" id="dynamic_field">  
                                                <tr>  
                                                <td><input required type="text" name="name[]" placeholder="" class="form-control name_list" /></td>  
                                                <td><button type="button" name="add" id="add" class="btn btn-success">+</button></td>  
                                                </tr> 
                                                </table>
    
                                
                                                </div>
                                            
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button class="btn btn-sm btn-primary" type="submit">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                    <div class="clearfix"></div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTAINER -->
        </div>
        <!-- /wrapper -->


        <!-- SCROLL TO TOP -->
        <a href="#" id="toTop"></a> 
        <script>
            $(document).ready(function(){      
            var postURL = "<?php echo url('addmore'); ?>";
           var i=1;  
                       
                       
           $('#add').click(function(){  
             i++;  
            $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input required type="text" name="name[]" placeholder="" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
            });  
                       
                       
           $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
           });  
            });  
           </script>
	@endsection
    