@extends('layouts.adm') 
@section('content')


<!-- BEGIN CONTENT BODY -->
                <div class="page-content-wrapper">
                    <div class="content-wrapper container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title">

                                    <h4 class="float-left">Notes Page</h4>

                                    <ol class="breadcrumb float-left float-md-right">
                                      <li class="breadcrumb-item"><a href="/notes"><i class="fa fa-arrow-left"></i></a></li>

                                    </ol>

                                </div>
                            </div>
                        </div><!-- end .page title-->

				
								<div class="col-md-12">
                                <div class="panel panel-card margin-b-30">
                                    <!-- Start .panel -->
                                   
                                       
                                        <div class="float-right">
                                            <div class="btn-group">
                                                
                                                <div class="dropdown-menu panel-dropdown" role="menu">
                                                    <a  class="dropdown-item" href="#">Action</a>
                                                    <a class="dropdown-item" href="#">Another action</a>
                                                    <a class="dropdown-item" href="#">Something else here</a>

                                                    <a class="dropdown-item" href="#">Separated link</a>
                                                </div>
                                            </div>
                                        </div>
                                 
                                    <div class="panel-body  p-xl-3">
                                        @foreach($data as $datas)
											 JUDUL : <b>{{$datas->judul}}</b>
											 <br>
											<div class="form-group row" style="background-color:white;height:600px;margin-top:10px">
											

                                                <div class="col-lg-12">
                                                	@php
									              echo $datas->note
									              @endphp	
                                                </div>
											</div>
											
											
										@endforeach
                                    </div>
                                </div>
				
				
				
				
				
				
				
				
            </div>
            <!-- END CONTAINER -->
        </div>
        <!-- /wrapper -->
		
		 

        <!-- SCROLL TO TOP -->
        <a href="#" id="toTop"></a> 
	@endsection