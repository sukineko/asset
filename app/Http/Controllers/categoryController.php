<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\category;
use App\Models\spec;

class categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = category::find(1)->spec;
        
        // dd($data);
        $data = category::all();
        return view('category.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new category();
        $data->category = $request->category;
        $data->save();

        $names = $request->input('name');
        foreach($names as $name) {
            $spec = new spec;
            $spec->name = $name;
            $spec->cat_id = $data->id;

            $spec->save();
        }

        return redirect()->route('category.index')->with('alert-success', 'Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = category::where('id', $id)->get();
        return view('category.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = category::where('id', $id)->first();
        $data->name = $request->name;
        $data->save();
        return redirect()->route('category.index')->with('alert-success', 'Data berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = category::where('id', $id)->first();
        $data->delete();
        return redirect()->route('category.index')->with('alert-success', 'Data berhasi dihapus!');
    }

    public function show($id)
    {
        $spec = spec::where('cat_id', $id)->get();
        
        return view('category.show', compact('spec'));
        
    }

}
