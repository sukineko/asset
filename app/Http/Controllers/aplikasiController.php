<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\server;
use App\Models\aplikasi;


class aplikasiController extends Controller
{
	
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	public function __construct(){
    	$this -> middleware('auth');    	
    	//$this -> middleware('akses:2');
    } 
	 
    public function index()
    {
        $data=aplikasi::paginate(11);
        
        return view('aplikasi.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$data['server'] = server::get();
		return view('aplikasi.create', $data);
		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		
        $data = new aplikasi;
        $data->server_id = $request->server_id;
		$data->name = $request->name;
        $data->server_id = $request->server_id;
		$data->ip = $request->ip;
        $data->root = $request->root;
        $data->rootpass = $request->rootpass;
        $data->username = $request->username;
        $data->userpass = $request->userpass;


	
        $data -> save();
        //request()->pic->move(public_path('assets/images'), $imageName);
        //return redirect('aplikasi');
		
		return redirect()->route('aplikasi.index')->with('alert-success', 'Berhasil Menambahkan Data!');
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data['aplikasi'] = aplikasi::where('id', $id)->get();
       $data['server'] = server::get();
	   return view('aplikasi.lihat', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       //$data = aplikasi::all();
       $data['aplikasi'] = aplikasi::where('id', $id)->get();
       $data['server'] = server::get();

	   return view('aplikasi.edit', $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		
		
        $data = aplikasi::where('id', $id)->first();
        $data->server_id = $request->server_id;
		$data->name = $request->name;
		$data->server_id = $request->server_id;
        $data->ip = $request->ip;
        $data->root = $request->root;
        $data->rootpass = $request->rootpass;
        $data->username = $request->username;
        $data->userpass = $request->userpass;
		$data->save();
		return redirect()->route('aplikasi.index')->with('alert-success', 'Data berhasil diubah!');
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = aplikasi::where('id', $id)->first();
		/**$picture = $data->pic;
        File::delete('images/'.$picture);
		*/
		aplikasi::find($id)->delete();
        return back();
    }
}
