<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Models\notes;

class notesController extends Controller
{
    
    public function index()
    {
        $data=notes::all();
        return view('notes.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('notes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new notes();
		$data->judul = $request->judul;
        $data->note = $request->note;
		$data->save();
		return redirect()->route('notes.index')->with('alert-success', 'Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function aplikasi($id)
    {
       $aplikasi= aplikasi::where('server_id', $id)->get();
      
	   return view('notes.aplikasi',compact('aplikasi'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = notes::where('id', $id)->get();
		return view('notes.edit', compact('data'));
    }
    public function show($id)
    {
        $data = notes::where('id', $id)->get();
		return view('notes.show', compact('data'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = notes::where('id', $id)->first();
		$data->judul = $request->judul;
        $data->note = $request->note;
		$data->save();
		return redirect()->route('notes.index')->with('alert-success', 'Data berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = notes::where('id', $id)->first();
		$data->delete();
		return redirect()->route('notes.index')->with('alert-success', 'Data berhasi dihapus!');
    }
}
