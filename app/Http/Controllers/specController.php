<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\category;
use App\Models\spec;


class specController extends Controller
{
	
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	public function __construct(){
    	$this -> middleware('auth');    	
    	//$this -> middleware('akses:2');
    } 
	 
    public function index()
    {
        $data=spec::paginate(11);
        
        return view('spec.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$data['category'] = category::get();
		return view('spec.create', $data);
		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	
            $names = $request->input('name');
            foreach($names as $name) {
                $data = new spec;
                $data->name = $name;
                $data->cat_id = $request->cat_id;

                $data->save();
            }

        
        //request()->pic->move(public_path('assets/images'), $imageName);
        //return redirect('spec');
		
		return redirect()->route('spec.index')->with('alert-success', 'Berhasil Menambahkan Data!');
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data['spec'] = spec::where('id', $id)->get();
       $data['category'] = category::get();
	   return view('spec.lihat', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
       //$data = spec::all();
       $data['spec'] = spec::where('id', $id)->get();
       $data['category'] = category::get();

	   return view('spec.edit', $data );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		
        $data = spec::where('id', $id)->first();
        $data->cat_id = $request->cat_id;
		$data->name = $request->name;
		$data->save();
		return redirect()->route('spec.index')->with('alert-success', 'Data berhasil diubah!');
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = spec::where('id', $id)->first();
		/**$picture = $data->pic;
        File::delete('images/'.$picture);
		*/
		spec::find($id)->delete();
        return back();
    }
}
