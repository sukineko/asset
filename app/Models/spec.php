<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class spec extends Model
{
    protected $table = 'spec';
    public $timestamps = true;

    public function category()
    {
        return $this->belongsTo('App\Models\category', 'cat_id');
    }
}
