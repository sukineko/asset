<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class asset extends Model
{
    protected $table = 'asset';
    public $timestamps = true;

    public function server()
    {
        return $this->belongsTo('App\Models\category', 'cat_id');
    }

    public function spec_value()
    {
        return $this->hasMany('App\Models\spec_value', 'id');
    }
}
