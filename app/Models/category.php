<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $table = 'category';
    protected $fillable = ['category'];


    public function asset()
    {
        return $this->hasMany('App\Models\asset', 'id');
    }

    public function spec()
    {
        return $this->hasMany('App\Models\spec', 'id');
    }
}
