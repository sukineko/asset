<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class spec_value extends Model
{
    protected $table = 'spec_value';
    public $timestamps = true;

    public function spec()
    {
        return $this->belongsTo('App\Models\spec', 'spec_id');
    }

    public function asset()
    {
        return $this->belongsTo('App\Models\asset', 'asset_id');
    }
}
