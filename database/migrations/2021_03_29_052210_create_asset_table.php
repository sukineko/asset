<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('asset')) {

            Schema::create('asset', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->date('date');
                $table->string('name');
                $table->unsignedInteger('user_id')->nullable();
                $table->timestamps();
            });
        }
        Schema::table('asset', function (Blueprint $table) {
            $table->foreignId('cat_id')->constrained('category')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset');
    }
}
